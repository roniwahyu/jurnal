Perancangan Dan Pembuatan Game Adventure Finding Diamond Menggunakan 
Unity 3D Berbasis Android
    Nahdia Asri Umami1, Ina Agustina2, Fauziah3
    Teknik Informatika, Fakultas Teknologi Komunikasi dan Informatika, Universitas Nasional
   nahdiaarumi@gmail.com, ina.agustina@civitas.unas.ac.id, fauziah@citivas.unas.ac.id

   Abstract - The development of gaming industry at this time very rapidly with various kinds of genre. One of the most popular games is game game adventure genre. The process and game design begins with the storyline design. In game development there are some software needed to develop the game such as unity 3d. Unity 3D itself is a tool that integrates to create games ranging from building architecture and simulation. Unity can be used for making games that can be used on android devices. Android is growing very rapidly in the smart phone market in the world. Based on the results of the study, a suitable Dynamic Weighting A* method is implemented in Game Advanture that recognizes the arena environment and does not happen the same random scramble at the time the application is reopened.
	
   Keywords:Dynamic Weighting A*, Advanture game, Unity 3D, Android

   Abstrak � Perkembangan industri game pada saat ini sangat pesat dengan berbagai macam jenis genre. Salah satu jenis game yang digemari adalah game bergenre adventure game.proses dan perancangan game dimulai dengan menggunakan desain alur cerita. Dalam pengembangan game ada beberapa software yang diperlukan untuk perangan mengembangkan game seperti unity 3d. Unity 3D sendiri adalah sebuah tool yang terintregasi untuk membuat game  mulai dari arsitektur bangunan dan simulasi. Unity dapat digunakan untuk pembuatan game yang bisa digunakan pada perangkat android. Android berkembang sangat pesat di pasaran ponsel pintar di dunia.  Berdasarkan hasil penelitian, metode  Algoritma Dynamic Weighting A*cocok diimplementasikan pada Game Advanture yang mengenal lingkungan arena  dan tidak terjadi perulangan pengacakan yang sama pada saat aplikasi dibuka kembali.
   Kata Kunci : Algoritma Dynamic Weighting A*, Advanture game, Unity 3D, Android
I. PENDAHULUAN
     Finding Diamond sebuah game yang .semua kalangan dimana permainan ini menekan kepada pemainnya untuk menyelesaikan. permainan, karena didalam permainan fining diaomond akan ada banyak rintangan yang harus dihadapi untuk memperoleh diamond.Game ini akan mengajak pemain menjelajah dan berpetualang yang berbeda 

	




disetiap levelnya. Game merupakan salah satu media hiburan yang menjadi pilihan masyarakat untuk menghilangkan kejenuhan atau hanya untuk sekedar mengisi waktu luang.
        Dengan kemajuan teknologi yang terus berkembang, perkembangan industri game pada saat ini sangat pesat dengan berbagai macam jenis game. Jenis-jenis game yang banyank diminati contohnya seperti game yang ber genre sport, action, puzzle, advandture. Dalam pembuatan game bergenre tentunya membutuhkan software yang digunakan untuk membuat game, saat ini banyak software yang memiliki kualitas dan fitur yang sangat canggih untuk membuat game.Unity 3D merupakan game engine yang berbasis cross-platform. Unity 3d merupakan software yang digunakan untuk mengembangkan game multi platform yang didesain. Unity dapat digunakan untuk membuat video game 3D, realtime animasi 3D dan visualisasi arsitektur.
        Rumusan masalah yang dapat dirangkum dari pengidentifkasian masalah diatas adalah bagaimana membuat adventure finding diamond game menggunakan unity 3D.
        Dalam membangun game ini diperlukan adanya batasan agar tidak menyimpang dari apa yang direncanakan sehingga tujuan dari game ini dapat tercapai. Batasan masalah yang diperlukan dalam game ini adalah :
1. Perancangan game yang dibuat untuk single player.
2. Game ini dirancang untuk dimainkan secara offline.
3. Aplikasi yang digunakan dalam pembuatan game ini menggunakan unity 3D.
Tujuan dari pembuatan game ini adalah sebagai berikut:
1. Sebagai sarana hiburan dan media pembelajaran  untuk melatih kecerdasan dan ketelitian terhadap suatu kondisi. 
2. Turut serta menghasilkan karya dibidang Teknologi Multimedia.
3. Melatih kreatifitas anak dalam berexplorasi didalam kesulitan






II. METDOLOGI PENELITIAN

2.1 Flow Chart Kerangka Penelitian
	Berikut ini adalah tampilan kerangka penelitian finding diamond  game.

Gambar 1. Flow Chart Kerangka Penelitian

2.2 Perancangan Tampilan
          Berikut ini adalah perancangan tampilan finding diamond game yang dibangun.

Gambar 2. Flow Chart  Perancangan Game

2.3 Algoritma Dynamic Weighting 
 	Dynamic Algoritma Weighting A* merupakan perkembangan dari A*. Algoritma ini bertujuan untuk melakukan pencarian kesegala arah pada awal iterasi dan ketika goal state  sudah dekat baru pencarian difokuskan ke arah goal state merupakan karakterisitik dari  algoritma dan penerapan algoritma dynamic weighting ini akan diterapkan dalam penyebaran titik untuk penempatan diamond disetiap level. Untuk itu digunakan juga pembobotan yang dinamis.. 
f(n) = g(n) + ( w(n) * h(n) )
Dengan :

f(n)= hasil perhitungan dari g(n),h(n), dan w(n)

n= simpul saat ini

g(n)= biaya (cost) dari simpul awal ke simpul n sepanjang jalur pencarian

h(n)= perkiraan cost dari simpul n ke simpul
tujuan (nilai heuristik)

w(n)= bobot dinamis

2.4 Perancangan Aplikasi
  Spesifikasi yang digunakan : 
1. Teknologi yang digunakan
* Bahas Pemrograman : C#, Java Script
* Game Engine : Unity 3D
* Software Pembangun 3D : Blender .
* Sistem Operasi : Windows 8
* Spesifikasi computer yang digunakan Processor : Intel(R) Celeron(R) CPU 1007U @ 1.50GHz 1.50GHz. RAM : 4096 GB (1.89 GB usable) 

2. Target Permainan 
           Pembuatan advanture finding diamond game ini ditargetkan untuk semua kalangan dari anak-anak hingga dewasa.
        
3. Informasi Game.
           Aplikasi advanture game yang dibuat diberi nama Finding Diamond. Game Finding Diamond Game merupakan sebuah game android yang dapat memberikan kontribusi kepada masyarakat dan anak-anak. Game ini memiliki spesifikasin sebagai berikut: 
* Game yang dibuat berjenis adventure game berbentuk 3 dimensi.
* Game dapat dijalankan pada android.
* Game bersifat single player 
           Ide dari game yang saya buat adalah melakukan pencarian diamond  di dalam setiap levelnya dan dapat memilih karakter laki-laki dan perem untuk memainkan game, di dalam game yang saya buat memiliki 3 level dimana di level pertama harus melakukan pencarian diamond dan melewati rintangan dengan karakter yang telah dipilih. Pencarian diamond ini dibatasi dengan 5 pencarin disetiap level dimana ketika user mendapatkan 1 diamond dalam setiap level sudah bisa dikatakan menang. Namun bila dalam pencarian diamond 
    
a. Storyboard Aplikasi
DESAINKETERANGAN
Splash Screen
Menu Tap To Start pilihan untuk kemenu untama.


Main Menu untuk memulai game, memberi informasi dan keluar dari game.
Menu Pemilihan karakter yang akan dimainkan setelah tombol play dipilih
Menu About berisi tentang informasi penulis.Gambar 3. Flow Chart Story Board

III. IMPLEMENTASI  DAN PENGUJIAN

3.1 Implementasi
       Implementasi ini merupakan tahapan untuk mengubah hasil dari rancangan system yang telah disusun berdasarkan Alur dan Storyboard menjadi bentuk nyata, dalam hal ini berupa aplikasi games yang berjalan pada platform android. Pemilihan pembuatan game ini bertujuan untuk mengembangkan game dimedia hiburan dibidang teknologi multimedia dengan judul perancangan dan pembuatan advanture finding diamond game berbasis android. 
1. Splash Screen
Gambar 4. Splash Screen
       Splash Screen merupakan tampilan awal pembuka aplikasi permainan yang sedang berjalan.
2. Scene Tap To Start
Gambar 5. Tap To Start
       Tap To Start merupakan tampilan pilihan untuk menuju ke main menu.
3. Scene Main Menu
Gambar 6. Main Menu
      Tampilan Main Menu merupakan tampilan utama dari aplikasi games ini. Di dalam halaman menu utamaini terdiri dari beberapa  tombol yang diakses untuk pengguna, yaitu button� play�, �About�, �Exit�.
4. Scene Pemilihan Karakter
Gambar 7.Pemilihan Karakter
       Pada form ini, user dapat memilih karakter yang akan diamainkan. Dalam form ini tedapat 2 tokoh karakter  dan terdapat tombol back yang berfungsi untuk kembali ke main menu.
5. Tampila Level 1
   Gambar 8.Level 1
       Pada tampian scene level 1 merupakan tampilan permainan tahap awal yang dimana user harus memasuki area labirin, dan harus mencari diamond -diamond  yang telah di tempatkan  pada titik tertentu serta harus menghindari jebakan  yang telah disiapkan. Pada level ini user minimal harus mendapatkan satu diamond untuk bisa melanjutkan kelevel selanjutnya. Apabila terkena jebakan pada level ini maka permainan akan mengulang dari awal.Dalam level 1 ini diterapkan Algoritma Dynamic Weighting A* untuk penyebaran titik-titik penempatan diamond yang telah ditentukan oleh pembuat atau penulis, dan penemuan goal  state atau finish yang ditandai dengan warna hijau yang menandakan bahwa gol state sudah dekat.Penerapannya adalah sebagai berikut.
    
6. Tampilan Level 2
Gambar 9.Level 2
      Pada tampilan scene level 2 merupakan tampilan permainan tahap kedua, pada level 2 ini tidak jauh berbeda dengan level 1, hanya saja  ada beberapa perbedaan yaitu pada tingkat kesulitan dilevel ini ditambah dengan adanya area labirin untuk pencarian diamond serta arah menuju finish lebih dibuat sedikit rumit. Namun berbeda dengan level 1 penerapan Algoritma Dynamic Weighting A* untuk penyebaran titik-titik penempatan diamond  dan penerapan goal state yang ditandai dengan warna yang sama yaitu hijau bahwa goal state yang dituju sudah dekat. Berikut adalah penerapan penyebaran pada  level 2:
      
7. Tampilan Level 3
Gambar 10.Level 3
      Pada tampilan scene level 3 merupakan tampilan permainan tahap ketiga, pada level 3 ini juga prinsipnya sebenarnya masih sama dengan level 1&2 yaitu untuk mecari diamond, Dimana  tingkat kesulitan dilevel 3 ini adalah penggabungan dari tingkat kesulitan pada level 1 dan kesulitan di level 2, yaitu dengan memperbanyak jebakan dan penambahan area labirin. Pada level 3 ini penerapan Algoritma Dynamic Weighting A* untuk penyebaran titik-titik penempatan diamond adalah sebagai berikut :
     
8. Scene Win
Gambar 11. Tampilan Win
        Tampilan scene ini merupakan tampilan apabila user menyelesaikan finish, dan akan muncul button  untuk kembali ke menu awal dan button untuk melanjutkan game berikutnya.
        
9. Scene Game Over

Gambar 12. Tampilan Game Over
      Tampilan scene ini merupakan tampilan game over apabila user terkena jebakan, dan akan muncul button  untuk kembali ke menu awal dan button untuk try again untuk memulai kembali game tersebut.
10. Scene Pause
Gambar 13. Tampilan Pause
      Tampilan scene ini merupakan tampilan pause apabila user ingin menjeda permainan saat sedang berjalan, dan akan muncul button  untuk kembali ke menu awal,button play untuk memulai game yang dijeda  dan button try again untuk memulai game dari awal.
11. Scene About
Gambar 14. Tampilan About
      Dalam form About ini berisi tentang informasi pembuat atau perancang  finding diamond game. .
     

3.2 Pengujian Game
       Dalam analisis pengujian sistem dilakukan menggunakan 2 smartphone dengan sistem operasi android. Untuk smartphone pertama yaitu berspesifikasi rendah,Smartphone kedua berspesifikasi sedang dan yang ketiga berspesifikasi tinggi.
3.3 Pengujian Game
       Dalam pengujian game ini menggunakan  pengujian untuk mengetahui berfungsinya aplikasi.
1.  Pengujian Spesifikasi Smartphone

Tabel 1. Spesifikasi Smartphone
Spesifikasi Smartphone 1     Merk Smarthphonem 1Samsung Grand PrimeRAM2 GBSistem OperasiLolipop
Tabel 2. Spesifikasi Smartphone
Spesifikasi Smartphone 2Merk Smarthphonem 2XiaomiRAM2 GBSistem OperasiMarshmallow
2. Pengujian Tingkat Kecepatan Di Smartphone
Gambar 15. StoryBoard
      Dari Pengujian diatas, bahwa tingkat kecepatan masing-masing smartphone memiliki tingkat kecepatan yang berbeda namun kecepatan yang paling baik untuk game ini menggunakan smartphone  yang berspesifikasi tinggi.
3. Pengujian Balck Bock
      Pengujian Black Box bertujuan mengetahui fungsi perangkat lunak dalam pengoprasian game dan hasil uji blackbox dari game.
  Gambar 16. Pengujian Black Box

      Berdasarkan tabel hasil uji coba blackbox, virtual button yang digunakan berjalan sesuai yang diharapkan oleh user atau pengguna.
IV. KESIMPULAN
      Berdasarkan tahapan-tahapan dalam perancangan dan pembuatan Perancangan Dan Pembuatan Advanture Finding Diamond Game Menggunakan Unity 3D Berbasis Android, Maka dapat disimpulkan  game yang dibuat secara keseluruhan berdasarkan tingkat pengujian dengan smartphone telah berhasil dijalankan. 
        UCAPAN TERIMAKASIH
      Penulis mengucapkan terimakasih kepada Allah SWT. Kedua orang tua, Teman-teman dan Dosen-dosen yang membantu memberi masukan dan arahan untuk menyelesaikan tugas akhir ini. 
        DAFTAR ACUAN
[1] Zaenal Arifin �Membangun Game   
Petualangan Sejarah Peninggalan Sunan Kudus Berbasis Android�. Fakultas Teknik, Universitas Muria Kudus.
[2]  analisis perbandingan algoritma dynamic   weighting a* dan algoritma simplified memory-bounded a* (sma*)untuk mencari langkah optimal dalam penyelesaian permainan peg solitaire
[3] Wahyu Pratama �Game Adventure Misteri Kotak Pandora�. Program Studi Teknik Informatika STMIK AMIKOM Purwokerto.
[4] Marzuki Farandi � Game Berbasis Advanture Sebagai Pendukung Pembelajaran Pengenalan Kata Bahasa Inggris Untuk Anak Usia Dini.
[5] Yeni Winarti � Perancangan Game Edukasi Petualangan Dengan Tema Who Wants To Be A Milionaire Tingkat Anak SD.
[6] Jainal Winandin � Pembuatan Aplikasi Game The Adventure Of Hard Pada Amikom Surakarta� AMIK Citra Darma Surakarta.
[7] Syifaul Fuada � Perancangan Game Petualangan Pramuka Berbasis Android. Pasca Sarjana S2 Teknik Elektro. Sekolah Tinggi eknik Elektro dan Informatika.
[8] Aji Gunadi, Hanif Al Fatta � Analisis Dan Pembuatan Game Petualangan Si Argo Berbasis Flash�. STMIK AMIKOM Yogyakarta.
[9] Sunarti,Selly, Rahmawati, dan Setia Wardani � Pengembangan Game Petualangan Si Bolang Sebagai Media 
[10] Pembelajaran Tematik Untuk Meningkatakan Motivasi Dan Prestasi Belajar Siswa Kelas V Sekolah Dasar. FKIP Universitas PGRI Yogyakarta.
[11] I Made Mertha Prayuda, I Putu   Agung Bayupati , A. A. Kt. Agung Cahyawan Wiranatha. �Rancang Bangun Game The Adventure Of Timun Mas Berbasis Android.Jurusan Teknologi Informasi, Fakultas Teknik. Universitas Udayana.
[12] Shanggun Dayan �Devlopment of Design of Unity Functionality on Android�.
[13] Aehyun Kim, Jaehwa Baen � Design and Devlopment of Smart Game Based On Multilatform (Unity 3D) Game Engine. Tongmyong University.
[14] Shronet Dhuri,Priyanka Zha, Perang Nahete Shreyash Khot & Prof. Mahavir Devmane �Game Devlopment for Android Device Using Unity 3D�.
[15] Abdul Aleem Shaikh �Devlopment of Dave 3D Andrid Gaming Application�.
[16] Alvaro E. Carasco �Acceptability of an Advanture video game in the treatmen of female adolescents with symptomp of depression�. University of Chile.
[17] Halff. M. Henry, 2005, Adventure Game for Science Education: Generative.
[18] Thongchai Kaewkiriya �A Design And Devlopment Of Learning Conten For Multimedia Technology Using Multimedia Game �Faculty of Information Technology, Thai Nichi Institute of Technology, Bangkok,Thailand
[19] H.Pe hi and M. Chung �A Computer Adventure Game Applied in E Learning�, International Conference on Intelligent Pervasive Computing, IEEE, 2007, pp. 446
[20] Creighton, R.H(2010).�Unity 3D Game Development by Example�. Packt Publishing.Birmingham.
